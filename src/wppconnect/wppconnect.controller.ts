import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { WppconnectService } from './wppconnect.service';

@Controller('wppconnect')
export class WppconnectController {
  constructor(private readonly wppconnectService: WppconnectService) {}

  @Post('send')
  async sendMessageChe(@Body() body: { to: string; message: string }) {
    console.log('cehck');
    const { to, message } = body;
    await this.wppconnectService.sendMessage(to, message);
    return { status: 'Message sent' };
  }

  
  @Post('logout')
  async sessionLogout() {
    await this.wppconnectService.sessionLogout();
  }

  @Get('get-chat-history')
  async getChatHistory(
    @Query('chatId') chatId: string,
    @Query('includeMe') includeMe: boolean,
    @Query('includeNotifications') includeNotifications: boolean,
  ) {
    return await this.wppconnectService.getChatHistory(
      chatId,
      includeMe,
      includeNotifications,
    );
  }

  @Get('list-chats')
  async listChats() {
    return await this.wppconnectService.listChats();
  }
}
