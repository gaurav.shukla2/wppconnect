import { Test, TestingModule } from '@nestjs/testing';
import { WppconnectController } from './wppconnect.controller';
import { WppconnectService } from './wppconnect.service';

describe('WppconnectController', () => {
  let controller: WppconnectController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WppconnectController],
      providers: [WppconnectService],
    }).compile();

    controller = module.get<WppconnectController>(WppconnectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
