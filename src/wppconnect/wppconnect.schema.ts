import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MessageDocument = Message & Document;

@Schema({ timestamps: true })
export class Message {
  @Prop()
  body: string;

  @Prop()
  rawPayload: string;

  @Prop()
  from: string;

  @Prop()
  to: string;

  @Prop()
  type: string;

  @Prop()
  media: string; 

//   @Prop()
//   contact: Record<string, any>; 
}

export const MessageSchema = SchemaFactory.createForClass(Message);
