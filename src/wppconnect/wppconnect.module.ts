import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WppconnectService } from './wppconnect.service';
import { WppconnectController } from './wppconnect.controller';
import { Message, MessageSchema } from './wppconnect.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Message.name, schema: MessageSchema }]),
  ],
  controllers: [WppconnectController],
  providers: [WppconnectService],
})
export class WppconnectModule {}
