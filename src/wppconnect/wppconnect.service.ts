import { Injectable, OnModuleInit } from '@nestjs/common';
import * as wppconnect from '@wppconnect-team/wppconnect';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Message, MessageDocument } from './wppconnect.schema';

@Injectable()
export class WppconnectService implements OnModuleInit {
  private client: any;
  private isLogged: boolean = false;
  private urlCode: string | null = null;
  private attempt: number = 0;
  private isInChat: boolean = false;
  private autoCloseInterval: any;
  private autoCloseCalled: boolean = false;
  private options: any = {}; 
  catchQR: any;
  page: any;
  catchLinkCode: any;
  statusFind: any;
  session: any;
  cancelAutoClose: any;

  constructor(
    @InjectModel(Message.name) private messageModel: Model<MessageDocument>
  ) {}

  async onModuleInit() {
    // Session Configuration
    const sessionConfig = {
      session: 'mySession',
      puppeteerOptions: {
        headless: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
      },
    };

    try {
      this.client = await wppconnect.create(sessionConfig);
      this.initializeListeners();
      await this.checkQrCode(); // Start QR code check process
    } catch (error) {
      console.error('Error initializing WhatsApp client:', error);
    }
  }

  private initializeListeners() {

    // Listener for incoming messages
    this.client.onMessage(async (message) => {
      const messageBody = message.body || 'undefined';
      this.logMessage('received', message.from, messageBody);

      // Save received message to MongoDB
      const newMessage = new this.messageModel({
        body: message.body,
        rawPayload: JSON.stringify(message),
        from: message.sender.id,
        type: 'received',
        contact: message.contact,
      });
      await newMessage.save();
    });

    // Listener for outgoing messages
    this.client.onAnyMessage(async (message) => {
      if (message.fromMe) {
        const messageBody = message.body || 'undefined';
        this.logMessage('sent', message.to, messageBody);

        // Save sent message to MongoDB
        const newMessage = new this.messageModel({
          body: message.body,
          rawPayload: JSON.stringify(message),
          to: message.sender.id,
          type: 'sent',
          contact: message.contact,
        });
        await newMessage.save();
      }
    });
  }

  // for message send with API
  async sendMessage(to: string, message: string) {
    try {
      const data = await this.client.sendText(to, message);
      this.logMessage('sent', to, message);

      // Save sent message to MongoDB
      const newMessage = new this.messageModel({
        body: message,
        rawPayload: JSON.stringify(data),
        from: to,
        type: 'sent',
        contact: data.contact,
      });
      await newMessage.save();
    } catch (error) {
      console.error(`Failed to send message to ${to}:`, error);
      throw error;
    }
  }

  // logout session
  async sessionLogout() {
    try {
      await this.client.logout();
      return { status: 'Logged out successfully' };
    } catch (error) {
      console.error('Error during logout:', error);
      throw error;
    }
  }

  // for chat history
  async getChatHistory(chatId: string, includeMe: boolean, includeNotifications: boolean) {
    try {
      const messages = await this.client.getMessages(chatId, {
        includeMe,
        includeNotifications,
      });

      if (!messages || messages.length === 0) {
        throw new Error(`No messages found for chat with ID ${chatId}`);
      }

      messages.forEach(message => {
        this.logMessage(message.fromMe ? 'sent' : 'received', chatId, message.body);
      });

      return messages;
    } catch (error) {
      console.error('check', error);
      throw error;
    }
  }

  // list all chats with IDs
  async listChats() {
    try {
      const chats = await this.client.getAllChats();
      chats.forEach(chat => {
        this.displayChatMessages(chat.id);
      });
      return chats;
    } catch (error) {
      console.error('Failed to list chats:', error);
      throw error;
    }
  }

  private async displayChatMessages(chatId: string) {
    try {
      const messages = await this.getChatHistory(chatId, true, true);
      messages.forEach(message => {
        const direction = message.fromMe ? 'Sent' : 'Received';
      });
    } catch (error) {
      console.error(`Failed to retrieve messages for chat ${chatId}:`, error);
    }
  }

  private logMessage(direction: 'sent' | 'received', toOrFrom: string, message: string) {
    const timestamp = new Date().toISOString();
  }

  // qr code function
  private async checkQrCode() {
    try {
      const result = await this.client.getQrCode();

      if (!result?.urlCode || this.urlCode === result.urlCode) {
        console.log('QR code not available or already scanned.');
        return;
      }

      this.urlCode = result.urlCode;
      this.attempt++;

      let qr = '';

      if (this.options.logQR || this.catchQR) {
        qr = await this.client.asciiQr(this.urlCode);
      }

      if (this.options.logQR) {
        this.log('info', `Waiting for QRCode Scan (Attempt ${this.attempt})...:\n${qr}`, { code: this.urlCode });
      } else {
        this.log('verbose', `Waiting for QRCode Scan: Attempt ${this.attempt}`, { code: this.urlCode });
      }

      this.catchQR?.(result.base64Image, qr, this.attempt, result.urlCode);

      this.client.on('authenticated', async () => {
        this.isLogged = true;
        console.log('Successfully authenticated.');
        await this.onSuccessfulConnection();
      });
    } catch (error) {
      console.error('Error while checking QR code:', error);
    }
  }

  private async onSuccessfulConnection() {
    console.log('Successfully connected to WhatsApp');
    await this.listChats();
  }

  protected async loginByCode(phone: string) {
    const code = await this.client.evaluateAndReturn(
      this.page,
      async ({ phone }) => {
        return JSON.parse(
          JSON.stringify(await this.client.WPP.conn.genLinkDeviceCodeForPhoneNumber(phone))
        );
      },
      { phone }
    );

    if (this.options.logQR) {
      this.log('info', `Waiting for Login By Code (Code: ${code})\n`, { code });
    } else {
      this.log('verbose', `Waiting for Login By Code`, { code });
    }

    this.catchLinkCode?.(code);
  }

  protected async checkInChat() {
    const inChat = await this.client.isInsideChat(this.page).catch(() => null);

    this.isInChat = !!inChat;

    if (!inChat) {
      return;
    }
    this.log('http', 'Connected');
    this.statusFind?.('inChat', this.session);
  }

  protected tryAutoClose() {
    if (this.autoCloseInterval) {
      this.cancelAutoClose();
    }

    if (
      (this.options.autoClose > 0 || this.options.deviceSyncTimeout > 0) &&
      !this.autoCloseInterval &&
      !this.page.isClosed()
    ) {
      this.log('info', 'Closing the page');
      this.autoCloseCalled = true;
      this.statusFind && this.statusFind('autocloseCalled', this.session);
      try {
        this.page.close();
      } catch (error) {}
    }
  }

  private log(arg0: string, arg1: string, arg2?: { code?: string }) {
    const timestamp = new Date().toISOString();
    console.log(`[${timestamp}] ${arg0.toUpperCase()}: ${arg1}`, arg2 ? JSON.stringify(arg2) : '');
  }
}
