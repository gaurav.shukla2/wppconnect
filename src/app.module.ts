import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WppconnectModule } from './wppconnect/wppconnect.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    WppconnectModule,
    MongooseModule.forRoot('mongodb://localhost:27017/gaurav'), 
  ],  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
